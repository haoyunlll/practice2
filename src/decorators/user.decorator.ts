import { ExecutionContext, createParamDecorator } from '@nestjs/common';
//创建自定义装饰器的工厂函数
export const User = createParamDecorator(
  (data: string, context: ExecutionContext) => {
    const request = context.switchToHttp().getRequest();
    if (!data) {
      return request.user;
    }
    return request.user ? request.user[data] : null;
  },
);