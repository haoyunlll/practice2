import { SetMetadata } from '@nestjs/common';
//为特定的路由或控制器设置元数据，指示这些路由或控制器不需要进行身份验证
export const NoAuth = () => SetMetadata('noAuth', true);